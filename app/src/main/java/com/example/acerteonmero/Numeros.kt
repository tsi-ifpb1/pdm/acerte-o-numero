package com.example.acerteonmero

import java.util.*

object Numeros {

    fun getNumeros(): List<Int> {
        val random = Random()
        var numeros = mutableListOf<Int>()

        var numero = random.nextInt(100) + 1

        var divisiveis = mutableListOf<Int>()

        var parouimpar = if (numero % 2 == 0) 0 else 1

        var quantidadeDivisores = 0

        for (item in numero downTo 1) {
            if (numero % item == 0) {
                quantidadeDivisores += 1
            }
        }

        for (item in 1 .. if (numero < 10) numero else 10) {
            if (numero % item == 0) {
                divisiveis.add(item)
            }
        }

        numeros.add(numero)
        numeros.add(parouimpar)
        numeros.add(quantidadeDivisores)

        for (item in divisiveis) {
            numeros.add(item)
        }

        return numeros
    }
}