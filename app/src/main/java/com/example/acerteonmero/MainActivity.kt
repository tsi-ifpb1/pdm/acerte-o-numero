package com.example.acerteonmero

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var tvDicaUm: TextView
    private lateinit var tvDicaDois: TextView
    private lateinit var tvDicaTres: TextView
    private lateinit var btReiniciar: Button
    private lateinit var btChute: Button
    private lateinit var etChute: EditText
    private var NUMERO: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.tvDicaUm = findViewById(R.id.tvDicaUm)
        this.tvDicaDois = findViewById(R.id.tvDicaDois)
        this.tvDicaTres = findViewById(R.id.tvDicaTres)
        this.btReiniciar = findViewById(R.id.btReiniciar)
        this.btChute = findViewById(R.id.btChute)
        this.etChute = findViewById(R.id.etChute)

        this.btReiniciar.setOnClickListener({ sortearNumero() })
        this.btChute.setOnClickListener({ apostarNumero() })

        this.sortearNumero()
    }

    override fun onResume() {
        super.onResume()

        this.sortearNumero()
    }

    fun sortearNumero() {
        var numeros = Numeros.getNumeros()

        this.NUMERO = numeros[0]

        Log.i("APP_ACERTE", numeros[0].toString())

        var num = ""
        for ( item in 3..(numeros.size - 1)) {
            num += "(${numeros[item]}) "
        }

        this.tvDicaUm.text = "É um número " + if (numeros[1] == 1) "ÍMPAR" else "PAR"
        this.tvDicaDois.text = if (num == "") "Nenhum divisor entre 1 e 10" else "Divisores entre 1 e 10:\n${num}"
        this.tvDicaTres.text = "Possui ${numeros[2]} divisores no total."
        this.etChute.text.clear()
    }

    fun apostarNumero() {
        if (this.etChute.text.toString() == "") {
            Toast.makeText(this, "Digite algum valor", Toast.LENGTH_SHORT).show()
        } else {
            var aposta = this.etChute.text.toString().toInt()

            if (aposta == this.NUMERO) {
                Toast.makeText(this, "Você acertou, parabéns!", Toast.LENGTH_SHORT).show()
                this.sortearNumero()
            } else {
                Toast.makeText(this, "Você errou, o número era ${this.NUMERO}", Toast.LENGTH_SHORT).show()
                this.sortearNumero()
            }
        }
    }
}
